import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import {createStore} from "redux";

import AppRouter from './routers/AppRouter';
import reducers from './reducers'

import './styles/app.scss';

ReactDOM.render(
  <Provider store={createStore(reducers)}>
    <AppRouter />
  </Provider>, document.getElementById('app'));
