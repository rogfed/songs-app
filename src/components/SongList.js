import React from 'react'
import {connect} from "react-redux"

import {selectSong} from "../actions"
import PropTypes from 'prop-types';

const SongList = ({songs, selectSong}) => {
  const renderList = () => {
    return songs.map((song, index) => (
      <div className='item' key={index}>
        <div className='right floated content'>
          <button className='ui button primary' onClick={() => selectSong(song)}>Select</button>
        </div>
        <div className='content'>
          {song.title}
        </div>
      </div>
    ))
  }

  return <div className='ui divided list'>{renderList()}</div>
}

SongList.propTypes = {
  songs: PropTypes.array.isRequired,
  selectSong: PropTypes.func.isRequired
}

const mapStateToProps = ({songs}) => ({
  songs
})

export default connect(mapStateToProps, {selectSong})(SongList)
